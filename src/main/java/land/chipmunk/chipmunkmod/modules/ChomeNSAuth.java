package land.chipmunk.chipmunkmod.modules;

import com.google.common.hash.Hashing;
import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.listeners.Listener;
import land.chipmunk.chipmunkmod.listeners.ListenerManager;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.minecraft.text.Text;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class ChomeNSAuth extends Listener {
    public static final ChomeNSAuth INSTANCE = new ChomeNSAuth();

    public final String id = "chomens_bot_verify";

    public ChomeNSAuth () {
        ListenerManager.addListener(this);
    }

    public void init () {}

    @Override
    public void chatMessageReceived(Text message) {
        final String authKey = ChipmunkMod.CONFIG.bots.chomens.authKey;

        if (authKey == null) return;

        final Component component = message.asComponent();

        if (!(component instanceof TextComponent)) return;

        final String id = ((TextComponent) component).content();

        if (!id.equals(this.id)) return;

        final List<Component> children = component.children();

        if (children.size() != 2) return;

        if (!(children.get(0) instanceof TextComponent)) return;

        final String hash = ((TextComponent) children.get(0)).content();

        final long time = System.currentTimeMillis() / 10_000;

        final String actual = Hashing.sha256()
                // very pro hash input
                .hashString(authKey + time, StandardCharsets.UTF_8)
                .toString()
                .substring(0, 8);

        if (!hash.equals(actual)) return;

        if (!(children.get(1) instanceof TextComponent)) return;

        final String selector = ((TextComponent) children.get(1)).content();

        final String toSendHash = Hashing.sha256()
                // very pro hash input
                .hashString(authKey + authKey + time + time, StandardCharsets.UTF_8)
                .toString()
                .substring(0, 8);

        final Component toSend = Component.text(id)
                        .append(Component.text(toSendHash));

        final String toSendString = GsonComponentSerializer.gson().serialize(toSend);

        System.out.println("Sending " + toSendString + " to " + selector);

        CommandCore.INSTANCE.run("tellraw " + selector + " " + toSendString);

        CustomChat.INSTANCE.resetTotal();
    }
}
