package land.chipmunk.chipmunkmod;

import com.google.gson.JsonObject;
import land.chipmunk.chipmunkmod.data.BlockArea;
import net.minecraft.util.math.BlockPos;


public class Configuration {
  public CommandManager commands = new CommandManager();
  public CommandCore core = new CommandCore();
  public Bots bots = new Bots();
  public CustomChat customChat = new CustomChat();
  public boolean fullbright = true; // unused, but it is here for old configs
  public String autoSkinUsername = "off";

  public static class CommandManager {
    public String prefix = ".";
  }

  public static class CommandCore {
    public BlockArea relativeArea = new BlockArea(new BlockPos(0, 0, 0), new BlockPos(15, 0, 15));
  }

  public static class Bots {
    public BotInfo hbot = new BotInfo("#", null);
    public BotInfo sbot = new BotInfo(":", null);
    public BotInfo chipmunk = new BotInfo("'", null);
    public ChomeNSBotInfo chomens = new ChomeNSBotInfo("*", null, null, null);
    public BotInfo kittycorp = new BotInfo("^", null);
    public TestBotInfo testbot = new TestBotInfo("-", null);
  }

  public static class ChomeNSBotInfo {
    public String prefix;
    public String key;
    public String authKey;
    public String formatKey;

    public ChomeNSBotInfo (String prefix, String key, String authKey, String formatKey) {
      this.prefix = prefix;
      this.key = key;
      this.authKey = authKey;
      this.formatKey = formatKey;
    }
  }

  public static class TestBotInfo {
    public String prefix;
    public String webhookUrl;

    public TestBotInfo (String prefix, String webhookUrl) {
      this.prefix = prefix;
      this.webhookUrl = webhookUrl;
    }
  }

  public static class BotInfo {
    public String prefix;
    public String key;

    public BotInfo (String prefix, String key) {
      this.prefix = prefix;
      this.key = key;
    }
  }

  public static class CustomChat {
    public JsonObject format;
  }
}
